function dx = hydro_ODE_v50_B2(x,Y)
dx = zeros(5,1);    % a column vector

global n gamma accu W_s_scaled q_geo_scaled r q_0 Psi_C A_mean y_A 
global z0 z4

H   = Y(1);
u   = Y(2);
v   = Y(3);
q   = Y(4);

z_b = z4*x.^4 + z0;
dz_b_dx = 4*z4*x.^3;

deps=1e-7;

[~, index_X]=min(abs(y_A-x));
if index_X==1
    A_actual = A_mean(1);
elseif index_X==length(y_A)
    A_actual = A_mean(end);
elseif y_A(index_X)<x
    index_L=index_X;
    index_R=index_X+1;
    A_actual=  (x-y_A(index_L))/(y_A(index_R)-y_A(index_L))*A_mean(index_L) ...
             + (y_A(index_R)-x)/(y_A(index_R)-y_A(index_L))*A_mean(index_R);
else
    index_L=index_X-1;
    index_R=index_X;
    A_actual=  (x-y_A(index_L))/(y_A(index_R)-y_A(index_L))*A_mean(index_L) ...
             + (y_A(index_R)-x)/(y_A(index_R)-y_A(index_L))*A_mean(index_R);
end


N=H+r*z_b-Psi_C;

tau_b=max(0,gamma*N);

N=max(sqrt(eps),H+r*z_b-Psi_C);

heat_diss=2^(-1/n) * A_actual * abs((v./H)+deps).^(n+1);

if heat_diss>0
    H_ct= H-sqrt(2/heat_diss);
else
    H_ct=0;
end

%if u<0
%    u=0;
%end

if H_ct>0
    Q_ice = heat_diss.*H_ct;
else
    Q_ice = (1/2*H.*heat_diss-1./H );
end

%dx(1) = - dz_b_dx;
dx(1) = ( accu*(1-abs(x)) )^(1/n) * H^( -(n+2)/n )*(1+tanh((abs(x)-W_s_scaled)/deps))/2 - dz_b_dx;
%dx(1) = ( accu*(1-abs(x)) )^(1/n) * A_actual^(-1/n) * H^( -(n+2)/n )*(1+tanh((abs(x)-W_s_scaled)/deps))/2 - dz_b_dx;
dx(2) = A_actual * v * abs(v).^(n-1) / H^n;
dx(3) = 2^(1/n)*(tau_b-H)*(1+tanh(-v/deps))/2;
dx(4) = tau_b.*u + Q_ice + q_geo_scaled + q_0/(sqrt(eps)+N)^3;
dx(5) = q_0/(sqrt(eps)+N)^3;