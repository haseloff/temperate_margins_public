close all
set(0,'DefaultFigureWindowStyle','docked')

%% no ridge plots
load('ODE_DATA/permeability_vs_W_v50_B2_no_ridge.mat')
disp(['min melt=' num2str(min(melt_rate*melt_scale*1000)) 'mW m^{-2}'])

subplot(6,3,2)
hold off
a1=area(-y_New*y_scale*1e-3,z_b*z_scale); hold on
plot(-y_New*y_scale/1e3,(H+z_b)*z_scale,'-k','LineWidth',2);
plot(-y_New*y_scale*1e-3,(H_ct+z_b)*z_scale,'Color',[0.1 0.7 0],'LineWidth',2);


contourf(-Y/1e3,Z,TEMP_dim,[-30:5:0],'LineStyle','none'); hold on
plot(-y_New*y_scale/1e3,Gamma_ct_dim+z_b*z_scale,'Color',[0.1 0.7 0],'LineWidth',2); hold off

set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)
ylim([min(z_b) max(H+z_b)]*1.1*z_scale)
xlim([-1 0]*W/1e3)
ylabel('$z$ [m]','interpreter','latex')
set(gca,'FontSize',14)
load('temp_colormap.mat');
set(gcf,'Colormap',cmap);
caxis([-Delta_T 0])
ylim([-750 450])

subplot(6,3,5)
hold off
plot(-y_New*y_scale*1e-3,u*u_scale*(365*24*60*60),'-k','LineWidth',2)
ylim([0 1]*max(u*u_scale*(365*24*60*60))*1.1)
xlim([-1 0]*max(y_scale*1e-3))
ylabel({'Velocity' '$u$ [m yr$^{-1}$]'},'interpreter','latex')
set(gca,'FontSize',14)

subplot(6,3,8)
plot(-y_New*y_scale*1e-3,N*N_scale*1e-3,'-k','LineWidth',2)
ylim([0 1]*max(N*N_scale*1e-3)*1.1)
xlim([-1 0]*W/1e3)
ylabel({'Effective' 'pressure' '$N$ [kPa]'},'interpreter','latex')
set(gca,'FontSize',14)

Q_ice_jb=Q_ice;
Q_ice_jb(H_ct==0)=0;

Q_ice_mb=Q_ice;
Q_ice_mb(H_ct>0)=0;

m_b=tau_b.*u + q_geo_scaled+Q_ice_mb;
j_b=Q_ice_jb;

subplot(6,3,11)
hold off
plot(-y_New*y_scale*1e-3,melt_rate/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2); hold on
plot(-y_New*y_scale*1e-3,m_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.b','LineWidth',1)
plot(-y_New*y_scale*1e-3,j_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.r','LineWidth',1); hold off
ylabel({'Melt rate' '[$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
l1=legend('$\dot m_b + j_b$','$\dot m_b$','$j_b$');
set(l1,'Location','northwest','interpreter','latex','box','off')
set(gca,'FontSize',14)

subplot(6,3,17)
plot(-y_New*y_scale*1e-3,-q*q_scale*365*24*60*60,'-k','LineWidth',2);
xlabel('$y$ [km]','interpreter','latex')
ylabel({'Lateral' 'water flux' '$q_y$ [m$^2$ yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

subplot(6,3,14)
plot(-y_New*y_scale*1e-3,-q_0./(sqrt(eps)+N).^3/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2);
ylabel({'Downstream' 'flux divergence' '$$\frac{\partial q_x}{\partial x}$$ [$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
xlabel('$y$ [km]','interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

pause(1)
%% flat bed plots
load('ODE_DATA/permeability_vs_W_v50_B2_flat_bed.mat')
disp(['min melt=' num2str(min(melt_rate*melt_scale*1000)) 'mW m^{-2}'])

subplot(6,3,3)
hold off
a1=area(-y_New*y_scale*1e-3,z_b*z_scale); hold on
plot(-y_New*y_scale/1e3,(H+z_b)*z_scale,'-k','LineWidth',2);
plot(-y_New*y_scale*1e-3,(H_ct+z_b)*z_scale,'Color',[0.1 0.7 0],'LineWidth',2);


contourf(-Y/1e3,Z,TEMP_dim,[-30:5:0],'LineStyle','none'); hold on
plot(-y_New*y_scale/1e3,Gamma_ct_dim+z_b*z_scale,'Color',[0.1 0.7 0],'LineWidth',2); hold off

set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)
ylim([min(z_b) max(H+z_b)]*1.1*z_scale)
xlim([-1 0]*W/1e3)
ylabel('$z$ [m]','interpreter','latex')
set(gca,'FontSize',14)
set(gcf,'Colormap',cmap);
c1=colorbar;
set(c1,'Location','eastoutside','FontSize',14,'FontName','Times')
title(c1,'$T$ [$^o$C]','FontSize',14,'interpreter','latex')
caxis([-Delta_T 0])
ylim([-750 450])

subplot(6,3,6)
hold off
plot(-y_New*y_scale*1e-3,u*u_scale*(365*24*60*60),'-k','LineWidth',2)
ylim([0 1]*max(u*u_scale*(365*24*60*60))*1.1)
xlim([-1 0]*max(y_scale*1e-3))
ylabel({'Velocity' '$u$ [m yr$^{-1}$]'},'interpreter','latex')
set(gca,'FontSize',14)

subplot(6,3,9)
plot(-y_New*y_scale*1e-3,N*N_scale*1e-3,'-k','LineWidth',2)
ylim([0 1]*max(N*N_scale*1e-3)*1.1)
xlim([-1 0]*W/1e3)
ylabel({'Effective pressure' '$N$ [kPa]'},'interpreter','latex')
set(gca,'FontSize',14)

Q_ice_jb=Q_ice;
Q_ice_jb(H_ct==0)=0;

Q_ice_mb=Q_ice;
Q_ice_mb(H_ct>0)=0;

m_b=tau_b.*u + q_geo_scaled+Q_ice_mb;
j_b=Q_ice_jb;

subplot(6,3,12)
hold off
plot(-y_New*y_scale*1e-3,melt_rate/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2); hold on
plot(-y_New*y_scale*1e-3,m_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.b','LineWidth',1)
plot(-y_New*y_scale*1e-3,j_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.r','LineWidth',1); hold off
ylabel({'Melt rate' '[$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

subplot(6,3,18)
plot(-y_New*y_scale*1e-3,-q*q_scale*365*24*60*60,'-k','LineWidth',2);
xlabel('$y$ [km]','interpreter','latex')
ylabel({'Lateral' 'water flux' '$q_y$ [m$^2$ yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

subplot(6,3,15)
plot(-y_New*y_scale*1e-3,-q_0./(sqrt(eps)+N).^3/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2);
ylabel({'Downstream' 'water flux' '$\frac{\partial q_x}{\partial x}$ [$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
xlabel('$y$ [km]','interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

pause(1)
%% full example plots
load('ODE_DATA/permeability_vs_W_v50_B2.mat')
disp(['min melt=' num2str(min(melt_rate*melt_scale*1000)) 'mW m^{-2}'])

subplot(6,3,1)
hold off
a1=area(-y_New*y_scale*1e-3,z_b*z_scale); hold on
plot(-y_New*y_scale/1e3,(H+z_b)*z_scale,'-k','LineWidth',2);
plot(-y_New*y_scale*1e-3,(H_ct+z_b)*z_scale,'Color',[0.1 0.7 0],'LineWidth',2);


contourf(-Y/1e3,Z,TEMP_dim,[-30:5:0],'LineStyle','none'); hold on
plot(-y_New*y_scale/1e3,Gamma_ct_dim+z_b*z_scale,'Color',[0.1 0.7 0],'LineWidth',2); hold off

set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)
ylim([min(z_b) max(H+z_b)]*1.1*z_scale)
xlim([-1 0]*W/1e3)
ylabel('$z$ [m]','interpreter','latex')
set(gca,'FontSize',14)
set(gcf,'Colormap',cmap);
caxis([-Delta_T 0])
ylim([-750 450])

subplot(6,3,4)
hold off
plot(-y_New*y_scale*1e-3,u*u_scale*(365*24*60*60),'-k','LineWidth',2)
ylim([0 1]*max(u*u_scale*(365*24*60*60))*1.1)
xlim([-1 0]*max(y_scale*1e-3))
ylabel({'Velocity' '$u$ [m yr$^{-1}$]'},'interpreter','latex')
set(gca,'FontSize',14)

subplot(6,3,7)
plot(-y_New*y_scale*1e-3,N*N_scale*1e-3,'-k','LineWidth',2)
ylim([0 1]*max(N*N_scale*1e-3)*1.1)
xlim([-1 0]*W/1e3)
ylabel({'Effective pressure' '$N$ [kPa]'},'interpreter','latex')
set(gca,'FontSize',14)

Q_ice_jb=Q_ice;
Q_ice_jb(H_ct==0)=0;

Q_ice_mb=Q_ice;
Q_ice_mb(H_ct>0)=0;

m_b=tau_b.*u + q_geo_scaled+Q_ice_mb;
j_b=Q_ice_jb;

subplot(6,3,10)
hold off
plot(-y_New*y_scale*1e-3,melt_rate/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2); hold on
plot(-y_New*y_scale*1e-3,m_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.b','LineWidth',1)
plot(-y_New*y_scale*1e-3,j_b*melt_scale/rho_w/L_h*1000*365*24*60*60,'-.r','LineWidth',1); hold off
ylabel({'Melt rate' '[mm yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
% l2=legend('$\rho_w L_h (\dot m + j_b)$','$\rho_w L_h \dot m$','$\rho_w L_h j_b$');
% set(l2,'Location','NorthWest','interpreter','latex','box','off')
set(gca,'FontSize',14)

subplot(6,3,16)
plot(-y_New*y_scale*1e-3,-q*q_scale*365*24*60*60,'-k','LineWidth',2);
xlabel('$y$ [km]','interpreter','latex')
ylabel({'Lateral flux' '$q_y$ [m$^2$ yr$^{-1}$]'},'interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)

subplot(6,3,13)
plot(-y_New*y_scale*1e-3,-q_0./(sqrt(eps)+N).^3/rho_w/L_h*melt_scale*1000*365*24*60*60,'-k','LineWidth',2);
ylabel({'Downstream flux' '$\frac{\partial q_x}{\partial x}$ [mm yr$^{-1}$]'},'interpreter','latex')
xlabel('$y$ [km]','interpreter','latex')
xlim([-1 0]*W/1e3)
set(gca,'FontSize',14)


%%
subplot(6,3,1)
t2=title('Ridge- and bed-controlled'); set(t2,'interpreter','latex')

subplot(6,3,2)
t1=title('Bed-controlled'); set(t1,'interpreter','latex')

subplot(6,3,3)
t3=title('Ridge-controlled'); set(t3,'interpreter','latex')

