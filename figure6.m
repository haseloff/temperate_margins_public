clear all
close all

set(0,'DefaultFigureWindowStyle','docked')

k_w_index=0;

for k_w_value=[6 7 8:0.5:11 11.25 11.5 11.75 12]
    k_w_index=k_w_index+1;
    if k_w_value<10
        load(['ODE_DATA/permeability_vs_W_v50_B2_flat_bed_poro_coupling_k_w_1e-0' num2str(k_w_value) '.mat']);
    else 
        load(['ODE_DATA/permeability_vs_W_v50_B2_flat_bed_poro_coupling_k_w_1e-' num2str(k_w_value) '.mat']);
        
    end
    
    tau_scale=rho*g*y_scale*sin_alpha;
    
    k_w_array(k_w_index)=10^(-k_w_value);
    tau_array(k_w_index)=max(abs(v)./H)*tau_scale/1000;
    q_int_array(k_w_index)=X_New(end,5);
    max_phi_array(k_w_index)=max(max(PHI_dim))*100;
    
    Q_ice_jb=Q_ice;
    Q_ice_jb(H_ct==0)=0;
    j_b=Q_ice_jb;
    
    [Gamma_ct_max, Gamma_max_index]=max(Gamma_ct_dim);
    j_b_max(k_w_index)=j_b(Gamma_max_index);
    
    max_Gamma_array(k_w_index)=Gamma_ct_max;
    
    y_start_temp = y_temp(find(Gamma_ct_dim>0,1,'first'));
    y_end_temp   = y_temp(find(Gamma_ct_dim>0,1,'last'));

    width_Gamma_array(k_w_index) = (y_end_temp - y_start_temp)*y_scale/1e3;
    
    mean_phi_array(k_w_index)=100*mean(PHI_dim(find(Z(:,Gamma_max_index)<Gamma_ct_dim(Gamma_max_index)+z_b(Gamma_max_index)*z_scale),Gamma_max_index));

end
%%

subplot(2,2,1)
hold off
semilogx(k_w_array,width_Gamma_array,'-^k','LineWidth',1,'MarkerSize',10,'MarkerFaceColor','m');
ylabel([{'Width of temperate' 'ice region [km]'}],'interpreter','latex');
xlabel('Permeability parameter $k_w$ [m$^2$]','interpreter','latex')
xlim([1e-12 1e-8])
ylim([1 5.1])
set(gca,'FontSize',16,'FontName','Times','XTick',10.^[-12:2:-6])


subplot(2,2,2)
hold off
semilogx(k_w_array,mean_phi_array,'-^k','MarkerSize',10,'LineWidth',1,'MarkerFaceColor','m'); hold on
ylabel([{'Average porosity' '$\bar{\phi}$ [$\%$]'}],'interpreter','latex');
ylim([0 10])
xlim([1e-12 1e-8])
xlabel('Permeability parameter $k_w$ [m$^2$]','interpreter','latex')
set(gca,'FontSize',16,'FontName','Times','XTick',10.^[-12:2:-6])


subplot(2,2,3)
hold off
semilogx(k_w_array,j_b_max/rho_w/L_h*melt_scale*1000*365*24*60*60,'-^k','MarkerSize',10,'LineWidth',1,'MarkerFaceColor','m'); hold on
ylabel([{'Melt water flux' 'max($j_b$) [mm yr$^{-1}$]'}],'interpreter','latex');
xlim([1e-12 1e-8])
xlabel('Permeability parameter $k_w$ [m$^2$]','interpreter','latex')
set(gca,'FontSize',16,'FontName','Times','XTick',10.^[-12:2:-6])


subplot(2,2,4)
hold off
semilogx(k_w_array,-q_int_array*melt_scale/(rho_w*L_h)*1e3*(365*24*60*60),'-^k','MarkerSize',10,'LineWidth',1,'MarkerFaceColor','m'); hold on
ylabel([{'Average melt water' 'excess $\Gamma$ [mm yr$^{-1}$]'}],'interpreter','latex');
xlim([1e-12 1e-8])
xlabel('Permeability parameter $k_w$ [m$^2$]','interpreter','latex')
set(gca,'FontSize',16,'FontName','Times','XTick',10.^[-12:2:-6])

load(['ODE_DATA/permeability_vs_W_v50_B2_flat_bed_poro_coupling_k_w_infty.mat']);
tau_scale=rho*g*y_scale*sin_alpha;

q_int_infty=X_New(end,5);
max_phi_infty=max(max(PHI_dim))*100;

Q_ice_jb=Q_ice;
Q_ice_jb(H_ct==0)=0;
j_b=Q_ice_jb;


[Gamma_ct_max, Gamma_max_index]=max(Gamma_ct_dim);
j_b_max_infty=j_b(Gamma_max_index);

max_Gamma_infty=Gamma_ct_max;

y_start_temp = y_temp(find(Gamma_ct_dim>0,1,'first'));
y_end_temp   = y_temp(find(Gamma_ct_dim>0,1,'last'));

width_Gamma_infty = (y_end_temp - y_start_temp)*y_scale/1e3;

mean_phi_infty=100*mean(PHI_dim(find(Z(:,Gamma_max_index)<Gamma_ct_dim(Gamma_max_index)+z_b(Gamma_max_index)*z_scale),Gamma_max_index));


subplot(2,2,1)
hold on
semilogx([1e-12 1e-8],width_Gamma_infty*[1 1],'--r','LineWidth',1);

subplot(2,2,3)
hold on
semilogx([1e-12 1e-8],j_b_max_infty/rho_w/L_h*melt_scale*1000*365*24*60*60*[1 1],'--r','LineWidth',1); 

subplot(2,2,4)
hold on
semilogx([1e-12 1e-8],-q_int_infty*melt_scale/(rho_w*L_h)*1e3*(365*24*60*60)*[1 1],'--r','LineWidth',1); 

subplot(2,2,1)
text(5e-9,4.8,'(a)','FontName','Times','FontSize',16,'interpreter','latex')
l1=legend('$A=A(T,\phi)$','$A=A(T,0)$');
set(l1,'interpreter','latex','box','off','Location','SouthEast')

subplot(2,2,2)
text(5e-9,9,'(b)','FontName','Times','FontSize',16,'interpreter','latex')

subplot(2,2,3)
text(5e-9,180,'(c)','FontName','Times','FontSize',16,'interpreter','latex')

subplot(2,2,4)
text(5e-9,5.9,'(d)','FontName','Times','FontSize',16,'interpreter','latex')
