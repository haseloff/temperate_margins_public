clear all
%close all

global n gamma accu r W_s_scaled q_0 q_geo_scaled Psi_C k_w
global A_mean y_A  
global z0 z4

delta_y=0.001;
y_A=[0:delta_y:1]';
A_mean=1 + 0*sin(pi*y_A);

% geometric scales
W   = 50.275*1e3; % m, ice stream/ridge half width
W_s = 27*1e3;  % m, ice stream half width
s_0 = 200;    % m, ice stream surface elevation


mu=0.5;
sin_alpha=1e-3;
% other parameters
accu_dim=0.05/(365*24*60*60); % m s^{-1}
A_initial = 2.5*1e-25; % s^{-1} Pa^{-3}
g =9.81; % m/s^2
n=3;
rho  = 910; % kg/m^3
rho_w=1000; % kg/m^3
k   = 2.3; % W m^{-1} K^{-1}
L_h = 334*1e3; % J kg^{-1}

K=1e4;

T_s=-26.5; % Celsius
T_m=0;   % Celsius

q_geo= 70e-3; % W m^{-2}
q_x  = 0; % W m^{-2}

eta_w=1.8e-3;% Pa s
k_w = 1e-12; % m^2 Pa^{-1} s^{-1}

Delta_T=T_m-T_s;

% scales
y_scale = W;

tau_s = rho*g*sin_alpha*y_scale;

z_scale = sqrt(k*Delta_T/( A_initial*tau_s^(n+1) ));

N_scale = rho*g*z_scale;
u_scale = A_initial*tau_s^n*y_scale;
q_scale = A_initial*tau_s^(n+1)*z_scale*y_scale/(rho_w*L_h);

melt_scale = (rho_w*L_h)*q_scale/y_scale;

heat_scale = A_initial^(-1/n)*u_scale^(1/n+1)/y_scale^(1/n+1);
viscosity_scale=A_initial^(-1/n)*u_scale^(1/n-1)/y_scale^(1/n-1);
% non-dimensional parameters
accu  = (n+2)/2*accu_dim/(rho*g)^n/A_initial*y_scale^(n+1)/z_scale^(2*n+2);
q_geo_scaled = q_geo/(rho_w*L_h)*y_scale/q_scale;
W_s_scaled   = W_s/y_scale;
gamma = mu/sin_alpha;
r     = rho_w/rho;

options = odeset('RelTol',1e-9,'AbsTol',[1 1 1 1 1]*1e-10);
warning('off','MATLAB:nearlySingularMatrix');
mkdir ODE_DATA
%% Calculate reference solution
s_c =  s_0/z_scale;
z4 = 0/z_scale;
z0 = -627.1600/z_scale;


H_C=s_c-z0;
u_C=650/(u_scale*(365*24*60*60));

disp(['%% H_C=' num2str(H_C*z_scale) ', u_C=' num2str(u_C*u_scale*(365*24*60*60))])

diff_u=1;
diff_q=1;

u_bisection=0;
q_bisection=0;

Psi_C_plus  = H_C-r*z0;
Psi_C_minus = 0;

q_0_plus  = .0001;
q_0_minus = -.0001;

q_0=(q_0_plus+q_0_minus)/2;

% First bisection loop
while diff_u>1e-7 && u_bisection<100

    u_bisection=u_bisection+1;
    Psi_C = (Psi_C_plus+Psi_C_minus)/2;

    [y_New,X_New] = ode23s(@hydro_ODE_v50_B2,[0 1],[H_C u_C sqrt(eps) 0 0],options);

    u_New = X_New(end,2);

    if u_New<=0
        Psi_C_plus  = Psi_C;
    else
        Psi_C_minus = Psi_C;
    end

    diff_u=abs(u_New);
    
    z_b  = z4*y_New.^4 + z0;

    figure(1)
    subplot(7,1,[1 2])
    hold off
    a1=area(-y_New*y_scale*1e-3,z_b*z_scale); hold on

    set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)

    plot(-y_New*y_scale*1e-3,(X_New(:,1)+z_b)*z_scale,'-k','LineWidth',2)
    xlim([-1 0]*max(y_scale*1e-3))
    ylim([-800 500])

    subplot(7,1,3)
    plot(-y_New*y_scale*1e-3,X_New(:,2)*u_scale*(365*24*60*60),'-k','LineWidth',2)
    ylim([0 1]*max(X_New(:,2)*u_scale*(365*24*60*60))*1.1)
    xlim([-1 0]*max(y_scale*1e-3))
    ylabel('u [m yr^{-1}]')
    
    H = X_New(:,1);
    z_b  = z4*y_New.^4 + z0;

    u = X_New(:,2);
    v = X_New(:,3);
    q = X_New(:,4);
    q_int = X_New(end,5);
    N   = -Psi_C+H+r*z_b;

    subplot(7,1,4)
    plot(-y_New*y_scale*1e-3,N*N_scale*1e-3,'-k','LineWidth',2)
    ylim([min(N) max(N)]*N_scale*1e-3*1.1)
    xlim([-1 0]*W/1e3)
    ylabel('N [kPa]')
    
    
    
end

disp(['  diff_u=' num2str(diff_u) ', N=' num2str(min(N))])
%%

while diff_q>1e-6 && q_bisection<100
    q_bisection=q_bisection+1;

    q_0=(q_0_plus+q_0_minus)/2;

    [y_New,X_New] = ode23s(@hydro_ODE_v50_B2,[0 1],[H_C u_C 0 0 0],options);

    diff_q=X_New(end,4);

    if diff_q>0
        q_0_plus=q_0;
    else
        q_0_minus=q_0;
    end
    diff_q=abs(diff_q);
    
    y_temp = [0:delta_y:1]';
    X_New = interp1(y_New,X_New,y_temp);
    y_New = y_temp;

    H   = X_New(:,1);
    z_b  = z4*y_New.^4 + z0;
    u = X_New(:,2);
    v = X_New(:,3);
    q = X_New(:,4);
    q_int = X_New(end,5);
    N   = -Psi_C+H+r*z_b;
    
    subplot(7,1,6)
    plot(-y_New*y_scale*1e-3,q*melt_scale*1000,'-k','LineWidth',2);
    xlabel('y [km]')
    ylabel('q [mW m^{-2}]')
    xlim([-1 0]*W/1e3)
   
end

disp(['  diff_q=' num2str(diff_q) ', N=' num2str(min(N))])

disp(['--> H_C=' num2str(H_C*z_scale) ': u_C=' num2str(u_C*u_scale*365*24*60*60) ': Psi_C=' num2str(Psi_C) ', q_0=' num2str(q_0)])

%% Fixed-point iteration with variable permeability
diff_A=10;
A_counter=0;

while diff_A>1e-1 && A_counter<1
    A_counter=A_counter+1;
    diff_inner=1;
    k_counter=0;
    disp(['%% A_counter=' num2str(A_counter)])

    while (diff_inner>1e-6 && k_counter<20)

        dq_0  = sqrt(eps);
        dPsi_C= sqrt(eps);

        [y_New,X_New]   = ode23s(@hydro_ODE_v50_B2,[0 1],[H_C u_C 0 0 0],options);
        q_0=q_0+dq_0;
        [y_q,X_q]   = ode23s(@hydro_ODE_v50_B2,[0 1],[H_C u_C 0 0 0],options);
        q_0=q_0-dq_0;
        Psi_C=Psi_C+dPsi_C;
        [y_Psi,X_Psi]   = ode23s(@hydro_ODE_v50_B2,[0 1],[H_C u_C 0 0 0],options);
        Psi_C=Psi_C-dPsi_C;

        f=[X_New(end,2)
           X_New(end,4)];

        Jaco=[ (X_q(end,2)-X_New(end,2))/dq_0   (X_Psi(end,2)-X_New(end,2))/dPsi_C
               (X_q(end,4)-X_New(end,4))/dq_0   (X_Psi(end,4)-X_New(end,4))/dPsi_C];

        delta=-inv(Jaco)*f;

        q_0   = q_0   + delta(1);
        Psi_C = Psi_C + delta(2);

        diff_inner=norm(f);
        disp(['   q_0=' num2str(q_0) ', q_int=' num2str(X_New(end,5)) ', Psi_C=' num2str(Psi_C) ', diff=' num2str(diff_inner)])

        k_counter=k_counter+1;
    end
    if  isnan(diff_inner) || k_counter==20
        break
    end

    X_New = interp1(y_New,X_New,y_temp);
    y_New = y_temp;

    H = X_New(:,1);
    z_b  = z4*y_New.^4 + z0;
    u = X_New(:,2);
    v = X_New(:,3);
    q = X_New(:,4);
    q_int = X_New(end,5);
    N   = -Psi_C+H+r*z_b;
    N(N<=sqrt(eps))=sqrt(eps);

    heat_diss=2^(-1/n) * A_mean .* (abs(v./H)).^(n+1);
    viscosity=2^(-1/n) * A_mean.^(1-2/n) .* (abs(v./H)).^(1-n);
    heat_diss(isnan(heat_diss))=0;

    H_ct=H-sqrt(2./heat_diss);
    H_ct(heat_diss<=0)=0;
    H_ct(H_ct<=0)=0;

    deps=1e-7;

    Q_ice=1/2*H.*heat_diss-1./H;
    Q_ice(H_ct>0)=heat_diss(H_ct>0).*H_ct(H_ct>0);

    Gamma_ct= H_ct;

    tau_b = gamma*N;
    tau_b(tau_b<0)=0;


    melt_rate  = tau_b.*u + Q_ice + q_geo_scaled;
    figure(1)
    subplot(7,1,[1 2])
    hold off
    a1=area(-y_New*y_scale*1e-3,z_b*z_scale); hold on
    plot(-y_New*y_scale/1e3,(H+z_b)*z_scale,'-k','LineWidth',2); 
    plot(-y_New*y_scale*1e-3,(H_ct+z_b)*z_scale,'-r','LineWidth',2); 

    set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)
    ylim([min(z_b) max(H+z_b)]*1.1*z_scale)
    xlim([-1 0]*W/1e3)
    ylabel('z [m]')

    subplot(7,1,3)
    hold off
    plot(-y_New*y_scale*1e-3,u*u_scale*(365*24*60*60),'-k','LineWidth',2)
    ylim([0 1]*max(u*u_scale*(365*24*60*60))*1.1)
    xlim([-1 0]*max(y_scale*1e-3))
    ylabel('u [m yr^{-1}]')

    subplot(7,1,4)
    plot(-y_New*y_scale*1e-3,N*N_scale*1e-3,'-k','LineWidth',2)
    ylim([0 1]*max(N*N_scale*1e-3)*1.1)
    xlim([-1 0]*W/1e3)
    ylabel('N [kPa]')

    subplot(7,1,5)
    hold off
    plot(-y_New*y_scale*1e-3,melt_rate*melt_scale*1000,'-k','LineWidth',2); hold on
    plot(-y_New*y_scale*1e-3,tau_b.*u*melt_scale*1000,'-.b','LineWidth',1)
    plot(-y_New*y_scale*1e-3,Q_ice*melt_scale*1000,'-.r','LineWidth',1); hold off
    ylabel({'melt rate' '[mW m^{-2}]'})
    xlim([-1 0]*W/1e3)
    l2=legend('$\rho_w L_h (\dot m + j_b)$','$\rho_w L_h \dot m$','$\rho_w L_h j_b$');
    set(l2,'Location','NorthWest','interpreter','latex','box','off')

    subplot(7,1,6)
    plot(-y_New*y_scale*1e-3,q*melt_scale*1000,'-k','LineWidth',2);
    xlabel('y [km]')
    ylabel('q [mW m^{-2}]')
    xlim([-1 0]*W/1e3)

    subplot(7,1,7)
    semilogy(-y_New*y_scale*1e-3,q_0./(sqrt(eps)+N).^3*melt_scale*1000,'-k','LineWidth',2);
    ylabel('q_x [mW m^{-2}]')
    xlabel('y [km]')
    xlim([-1 0]*W/1e3)

    % temperate ice properties

    H_dim = H*z_scale;
    s_dim = H_dim;
    H_ct_dim     = H_ct*z_scale;
    Gamma_ct_dim = H_ct_dim;
    heat_diss_dim=heat_diss*heat_scale;
    viscosity_dim=viscosity*viscosity_scale;
    N_dim = N*N_scale;

    deps=1e-7;


    deltaZ=0.0001;
    z_temp=[0:deltaZ:1];
    [Y,Z]=meshgrid(y_New,z_temp);
    Y=Y*y_scale;

    THICKNESS_dim = NaN*Z;
    GAMMA_dim = NaN*Z;
    HEAT_dim = NaN*Z;
    TEMP_dim = NaN*Z;
    PHI_dim = NaN*Z;
    PE_dim = NaN*Z;

    alpha=0*Gamma_ct_dim;
    beta =0*Gamma_ct_dim;
    kappa=0*Gamma_ct_dim;
    critical_value=0*Gamma_ct_dim;


    for i=1:length(y_New)
        Z(:,i)=H_dim(i)*Z(:,i);
        S(:,i)=H_dim(i) + 0*Z(:,i);
        GAMMA_dim(:,i)=Gamma_ct_dim(i) + 0*Z(:,i);
        THICKNESS_dim(:,i)=H_dim(i) + 0*Z(:,i);
        HEAT_dim(:,i)=heat_diss_dim(i) + 0*Z(:,i);
        if Gamma_ct_dim(i)>0

            options_pe = odeset('RelTol',1e-8,'AbsTol',[1e-12 1e-12 1e-12 1e-12]);
            [z_pe,PE_fields]   = ode23s(@porosity_ODE_v48,[0 Gamma_ct_dim(i)],[N_dim(i) viscosity_dim(i) heat_diss_dim(i) Gamma_ct_dim(i)],options_pe);

            z_array = H_dim(i)*[0:deltaZ:Gamma_ct(i)/H_C]';
            p_e = interp1(z_pe,PE_fields(:,1),z_array);
            PE_dim(1:length(z_array),i)=p_e;
            PHI_dim(1:length(z_array),i)=viscosity_dim(i)./p_e * heat_diss_dim(i)/rho_w/L_h;
            for j=1:length(z_temp)
                if z_temp(j)*H_dim(i)>=Gamma_ct_dim(i)
                    TEMP_dim(j,i)=T_s + HEAT_dim(j,i)/k.*((S(j,i).^2-Z(j,i).^2)/2-GAMMA_dim(j,i).*(S(j,i)-Z(j,i))  );
                    %PHI_dim(:,i)  = 0;
                else
                    TEMP_dim(j,i)=T_m;
                end
            end

            alpha(i)=N_dim(i)/Gamma_ct_dim(i)/(rho_w-rho)/g;
            beta(i)=viscosity_dim(i)*heat_diss_dim(i)/(rho_w*L_h)/N_dim(i);
            kappa(i)=rho_w*L_h/heat_diss_dim(i)/Gamma_ct_dim(i)*k_w/eta_w*(rho_w-rho)*g;

            critical_value(i)=alpha(i)*beta(i)*kappa(i)^(1/3)/3;

        else
            TEMP_dim(:,i) = T_s + (T_m-T_s)*(S(:,i)-Z(:,i))./THICKNESS_dim(:,i)+ HEAT_dim(:,i)/(2*k).*(  (S(:,i)-Z(:,i)).*Z(:,i)  );
            %PHI_dim(:,i)  = 0;
        end
    end
    
    for i=1:length(y_New)
        Z(:,i)=Z(:,i)+z_b(i)*z_scale;
    end
    figure(1)
    subplot(7,1,[1 2])
    contourf(-Y/1e3,Z,TEMP_dim,'LineStyle','none'); hold on
    plot(-y_New*y_scale/1e3,Gamma_ct_dim+z_b*z_scale,'r','LineWidth',2); hold off
    c1=colorbar;
    set(c1,'Location','northoutside','FontSize',12)
    title(c1,'T [^oC]','FontSize',12)
    caxis([-Delta_T 0])
    ylim([-750 450])

    Temp_matrix=TEMP_dim+273.15;

    % Calculate the mean viscosity
    R= 8.314; % Gas constant J/ (mol K)

    A_array = 3.5e-25*exp(-6e4/R*(-1/263+1./Temp_matrix));
    A_array(Temp_matrix>263) = 3.5e-25*exp(-115e3/R*(-1/263+1./Temp_matrix(Temp_matrix>263)));

    A_array_no_temp=A_array;
    A_array_lin_fit=A_array;

    A_array_no_temp(~isnan(PHI_dim)) = 3.5e-25*exp(-115e3/R*(-1/263+1./273.15));
    A_array_lin_fit(~isnan(PHI_dim)) = 5.8e-24*PHI_dim(~isnan(PHI_dim))*100 + 2.47e-24;

    A_mean_no_poro=mean(A_array_no_temp.^(-1/n)).^(-n)';
    A_mean_lin_fit=mean(A_array_lin_fit.^(-1/n)).^(-n)';

    diff_A = norm(A_mean-A_mean_lin_fit/A_initial);
    A_mean = 0.75*A_mean + 0.25*A_mean_lin_fit/A_initial;

    disp(['---> diff_A=' num2str(diff_A) ' after ' num2str(A_counter) ' iterations'])
    disp(['     max(phi)=' num2str(max(max(PHI_dim)))])
end

save('ODE_DATA/permeability_vs_W_v50_B2_flat_bed.mat')