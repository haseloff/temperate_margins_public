clc
n=3;

YQ = (-50.27:0.25:0)*1000;
ZQ=  [-750:2.5:364.1];
[y,z] = meshgrid(YQ,ZQ);

A_rate=2.5e-25;
rho=910;
g=9.81;
W_s=27000;
W_m=29240;
W  =50280;
u_c=650/(365*24*60*60);
accu=0.05/(365*24*60*60);
u=u_c*(n+2)/(n+1)*( 1-(abs(y)/W_m).^(n+1) );
H_c= 827.2000;
z_b=-627.2;
H= H_c^(2+2/n) + 2*( (n+2)*accu/(2*A_rate*(rho*g)^n) )^(1/n).*((W-W_s)^(1+1/n)-(W-abs(y)).^(1+1/n)) ;
H=H.^(1/(2+2/n)); 
    H(abs(y)<W_s)=H_c;
velo_anal_s=1./H.*accu.*y.*(1 - W/W_s/(n+1).*( n+2 - (abs(y)/W_s).^(n+1) )  );

velo_anal_s(abs(y)>W_s)=accu*(W-abs(y(abs(y)>W_s)))./H(abs(y)>W_s);

velo_profile=(n+2)/(n+1)*(1 - (1-(z-z_b)./H).^(n+1) );
velo_anal_r=velo_anal_s.*velo_profile;
f = (abs(y)-W_s)/(W_m-W_s);
	f(abs(y)<W_s)=0;
	f(abs(y)>W_m)=1;

velo_anal_v=(1-f).*velo_anal_s + f.*velo_anal_r;
velo_anal_v(z>H+z_b)=NaN;
velo_anal_v(z<z_b)=NaN;


H= H_c^(2+2/n) + 2*((n+2)*accu/(2*A_rate*(rho*g)^n))^(1/n)*((W-W_s)^(1+1/n)-(W-abs(y)).^(1+1/n)) ;
H=H.^(1/(2+2/n));
    H(abs(y)<W_s)=H_c;
zeta=(z-z_b)./H;
velo_anal_s=-accu*zeta;
velo_anal_r_1  = -accu*(n+2)/(n+1)*(zeta-(1-(1-zeta).^(n+2))/(n+2));
velo_anal_r_2a = -( (n+2)/(2*A_rate*(rho*g)^n) )^(1/n);
velo_anal_r_2b = accu^(1+1/n)./H.^(2+2/n).*(W-abs(y)).^(1+1/n).*zeta.*(1-(1-zeta).^(n+1));
velo_anal_r_2 =velo_anal_r_2a .* velo_anal_r_2b;
velo_anal_r=velo_anal_r_1+velo_anal_r_2;



f = (abs(y)-W_s)/(W_m-W_s);
	f(abs(y)<W_s)=0;
	f(abs(y)>W_m)=1;

velo_anal_w=(1-f).*velo_anal_s + f.*velo_anal_r;

velo_anal_w(z>H+z_b)=NaN;
velo_anal_w(z<z_b)=NaN;

