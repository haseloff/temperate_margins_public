run figure3_a.m

load('ODE_DATA/permeability_vs_W_v50_B2_full_kappa_d_deps_1e-4.mat')

h_w=1; % in m
eta_w=1.8e-3;% in Pa s
N_0=1e6; % in Pa
k_d_want=2.5e-18;
kappa_want=k_d_want/(eta_w/h_w*(N_0/(rho*g*z_scale))^(-3)*A_initial*tau_s^(n+1)*z_scale^2/rho_w/L_h/rho/g);
[~, index_kappa]=min(abs(kappa_array-2.0504e-04));
kappa=kappa_array(index_kappa);
k_d=kappa*eta_w/h_w*(N_0/(rho*g*z_scale))^(-3)*A_initial*tau_s^(n+1)*z_scale^2/rho_w/L_h/rho/g;

disp(['k_d=' num2str(k_d) ' m^2$ at $N=' num2str(N_0) '$ Pa'])

q_0   = q_0_array(index_kappa);
[y_0,X_0]   = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C_array(index_kappa) 0 0],options);

%% Assign values
%N_inf=N_inf-r*z0+r*(z4*y_inf.^4 + z0);
tau_b_inf = gamma*N_inf;
tau_b_inf(tau_b_inf<0)=0;
melt_rate_inf  = tau_b_inf.*u_inf + Q_ice_inf + q_geo_scaled;

H = X_0(:,1);
        z_b  = z4*y_0.^4 + z0;
        u = X_0(:,2);
        v = X_0(:,3);
        Psi = X_0(:,4);
        q = X_0(:,5);
        q_int = X_0(end,6);
        N   = -Psi+H+r*z_b;
        y=y_0;
%N(N<=sqrt(eps))=sqrt(eps);
heat_diss=2^(-1/n) * (abs(v./H)).^(n+1);
viscosity=2^(-1/n) .* (abs(v./H)).^(1-n);
heat_diss(isnan(heat_diss))=0;

H_ct=H-sqrt(2./heat_diss);
H_ct(heat_diss<=0)=0;
H_ct(H_ct<=0)=0;

deps=1e-7;

Q_ice=1/2*H.*heat_diss-1./H;
Q_ice(H_ct>0)=heat_diss(H_ct>0).*H_ct(H_ct>0);

Gamma_ct= H_ct;

tau_b = gamma*N;
tau_b(tau_b<0)=0;

melt_rate  = tau_b.*u + Q_ice + q_geo_scaled;

%% Make final plots

    subplot(6,3,4)
    hold on
    plot(-y_0*y_scale,u*u_scale,':m','LineWidth',2); hold on
    
    subplot(6,3,7)
    hold on
    plot(-y*y_scale,N*N_scale,':m','LineWidth',2); hold on

    subplot(6,3,10)
    hold on
    plot(-y*y_scale,melt_rate/rho_w/L_h*melt_scale*1000*365*24*60*60,':m','LineWidth',2); hold on
  
    subplot(6,3,13)
    hold on
    plot(-y*y_scale,-q_0./(sqrt(eps)+N).^3/rho_w/L_h*melt_scale*1000*365*24*60*60,':m','LineWidth',2); hold on

    subplot(6,3,16)
    hold on
    plot(-y*y_scale,-q*q_scale*365*24*60*60,':m','LineWidth',2); hold on
