Matlab and Elmer/Ice source code files for a cross-sectional ice stream model. 

To reproduce figures, run files figure3.m, figure4.m, figure5.m, figure6.m, and figure7.m. Note that required input data is not included in this submission due to size constraints.

To reproduce data create folder ODE_DATA and run the following Matlab scripts:

permeability_vs_W_v50_B2.m (full geometry), 

permeability_vs_W_v50_B2_flat_bed.m (geometry with flat bed), 

permeability_vs_W_v50_B2_no_ridge.m (geometry without ice ridge), 

permeability_vs_W_v50_B2_full_kappa_d_deps_1e_4.m (finite bed permeability), 

permeability_vs_W_v50_B2_flat_bed_poro_coupling.m (with thermoviscous coupling). 
Run stream_z.sif and stream_z_advection.sif for Elmer/Ice calculations.
