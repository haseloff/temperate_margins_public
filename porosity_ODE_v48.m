function dx = porosity_ODE_v48(z,Y)
dx = zeros(4,1);  

global k_w

% constants
g     = 9.81; % m/s^2
rho   =  910; % kg/m^3
rho_w = 1000; % kg/m^3
L_h   =  334*1e3; % J kg^{-1}
eta_w = 1.8e-3; % Pa s

p_e        = Y(1);
eta_y      = Y(2);
heat_y     = Y(3);
Gamma_ct_y = Y(4);

phi=eta_y/p_e * heat_y/rho_w/L_h;
%g_phi=1./phi_array;

dx(1) = (rho_w-rho)*g - eta_w/k_w/phi^3*heat_y/rho_w/L_h*(Gamma_ct_y-z);
dx(2) = 0;
dx(3) = 0;
dx(4) = 0;