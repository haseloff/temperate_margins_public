%1:3 Velocity
%4:  Pressure scalar: 
%5:  W scalar
%6:  Temp scalar
%7:  Temp.homologous scalar
%8:  Temp.residual scalar
%9:  Temp.loads scalar
%10: Dtdz
clear all
close all
figure(5)

ii=0;
for kk=[3 2]
    ii=ii+1;
    u_scale=365*24*60*60;

    set(0,'DefaultFigureWindowStyle','docked') 

    if kk<10
        dataID=['00' num2str(kk)];
    else
        dataID=['0' num2str(kk)];
    end
    nameImport=['ELMER_DATA/mesh3d_za/stream_Pe_'  dataID '.ep'];    
    [a b]=grep('-n -e', '#time',nameImport);
    
    A = importdata(nameImport,' ',2);
    coordinates=A.data;

    B = importdata(nameImport,' ',b.line(end));
    variables=B.data;

    Y = (-50.27:0.25:0)*1000;
    Z=  [-750:0.25:364.1];
    [yq,zq] = meshgrid(Y,Z);
        
    coordinates_x(:,1)=coordinates(~isnan(coordinates(:,1)),1 );
    coordinates_y(:,1)=coordinates(~isnan(coordinates(:,2)),2 );
    coordinates_z(:,1)=coordinates(~isnan(coordinates(:,3)),3 );
    variables_T(:,1)=variables(~isnan(variables(:,1)),19 );
    
    Temp = griddata(coordinates_y,coordinates_z,variables_T,yq,zq,'linear');
    
    subplot(2,2,2+ii)
    hold off
    h2=area([min(Y/1000) max(Y/1000)],[min(Z) min(Z)],'LineWidth',2,'HandleVisibility','off'); hold on
    c1=contourf(yq/1000,zq,Temp,[-30.001:5:0],'LineStyle','none','HandleVisibility','off'); hold on
    [M2, c2]=contour(yq/1000,zq,Temp,[0 0],'k','LineWidth',2,'HandleVisibility','off'); hold on
    set(c2,'Visible','off')
%      c2_sorted=sortrows(c2.ContourMatrix.',1);
%      [C,ia,idx] = unique(c2_sorted(:,1),'stable');
%      val = accumarray(idx,c2_sorted(:,2),[],@mean); 
%      c2_averaged = [C val];
%      plot(c2_averaged(1:end-1,1),c2_averaged(1:end-1,2),'r','LineWidth',2)
% 
%     plot(c2_averaged(K,1),c2_averaged(K,2),'r','LineWidth',2)

    c2_sorted=sortrows(c2.ContourMatrix.',1);
    indices=find(c2_sorted(:,1)~=0);
    c2_sorted=c2_sorted(indices,:);

    deltaX=1;
    X1=c2_sorted(1,1);
    X2=c2_sorted(end,1);
    intervalN=abs(X2-X1)/deltaX;
    clear c2_averaged
    c2_averaged(1,1)=min(Y/1000);
    c2_averaged(1,2)=min(coordinates_z(:,1));
    c2_averaged(2,1)=X1;
    c2_averaged(2,2)=min(coordinates_z(:,1));
    for i=0:intervalN
        inDeltaX=find(c2_sorted(:,1)>=(X1+i*deltaX) & c2_sorted(:,1)<(X1+(i+1)*deltaX));
        c2_averaged(i+3,1)=mean(c2_sorted(inDeltaX,1));
        c2_averaged(i+3,2)=mean(c2_sorted(inDeltaX,2));
    end
    c2_averaged(i+4,1)=X2;
    c2_averaged(i+4,2)=min(coordinates_z(:,1));
    c2_averaged(i+5,1)=max(Y/1000);
    c2_averaged(i+5,2)=min(coordinates_z(:,1));
    hold on
%    plot(c2_averaged(:,1),c2_averaged(:,2),'r','LineWidth',3)
    plot(c2_averaged(2:end-1,1),c2_averaged(2:end-1,2),'-r','LineWidth',2)
    
    if kk==3
        title('Without lateral advection','interpreter','latex')
        ylabel('$z$ [m]','interpreter','latex')
    elseif kk==2
        title('With lateral advection','interpreter','latex')

        h=colorbar; %('Location','SouthOutside');
        set(h,'FontSize',16);
        title(h,'$T$ [$^o$C]','interpreter','latex')
        set(gca,'YTickLabel',[])
    end    

    xlabel('$y$ [km]','interpreter','latex')
    caxis([-26.5 0])
    ylim([-700 400])
    
    load_surfname=['ELMER_DATA/surface_00' num2str(kk) '.dat'];
    surfinterface=load(load_surfname);

    h1=area(surfinterface(:,5)/1000,surfinterface(:,6));
    set(h1,'BaseValue',400,'FaceColor','w','HandleVisibility','off')
    set(h2,'HandleVisibility','off','FaceColor',[1 1 1]*0.5)

%         load_bedname=['../bedinterface_00' num2str(kk) '.dat'];
%         bedinterface=load(load_bedname);
%         plot(bedinterface(:,5)/1000,bedinterface(:,6),'r','LineWidth',2);
        
end
%%
subplot(2,3,1)
hold off
figure7_velo_x

c6=colorbar; 
set(c6,'FontSize',16,'Location','NorthOutside','Ticks',[0 650])
title(c6,'$u$ [m year$^{-1}$]','interpreter','latex')
    
caxis([0 650])
ylim([-700 400])
set(gca,'box','on','XTick',[-50:10:0]*1000,'XTickLabel',[-50:10:0])
xlabel('$y$ [km]','interpreter','latex')
ylabel('$z$ [m]','interpreter','latex')

%%
figure7_analytic_profiles

subplot(2,3,2)
hold off
contourf(y,z,velo_anal_v*(365*24*60*60),[0:0.01:2],'LineStyle','none'); hold on
h3=area([-W 0],[z_b z_b],'LineWidth',2,'HandleVisibility','off');
q3=quiver(y(55:60:end,[2 25 55:35:end]),z(55:60:end,[2 25 50:35:end]),velo_anal_v(55:60:end,[2 25 50:35:end])*(365*24*60*60),velo_anal_w(55:60:end,[2 25 50:35:end])*(365*24*60*60));

load_surfname=['ELMER_DATA/surface_00' num2str(kk) '.dat'];
surfinterface=load(load_surfname);
plot(surfinterface(:,5),surfinterface(:,6),'k');

c3=colorbar;

set(q3,'Color','k','ShowArrowHead','off','Visible','on')
set(h3,'HandleVisibility','on','FaceColor',[1 1 1]*0.5,'BaseValue',-1000,'LineStyle','none')
set(c3,'Location','NorthOutside','FontSize',16,'Ticks',[0 1.6]);
title(c3,'$v$ [m year$^{-1}$]','interpreter','latex')

caxis([0 1.6])
ylim([-700 400])
set(gca,'box','on','XTick',[-50:10:0]*1000,'XTickLabel',[-50:10:0])
xlabel('$y$ [km]','interpreter','latex')


%%
subplot(2,3,3)
hold off
contourf(y,z,velo_anal_w*(365*24*60*60),[-.1:0.001:0],'LineStyle','none'); hold on
h4=area([-W 0],[z_b z_b],'LineWidth',2,'HandleVisibility','off');
q4=quiver(y(55:60:end,[2 25 55:35:end]),z(55:60:end,[2 25 50:35:end]),velo_anal_v(55:60:end,[2 25 50:35:end])*(365*24*60*60),velo_anal_w(55:60:end,[2 25 50:35:end])*(365*24*60*60));

load_surfname=['ELMER_DATA/surface_00' num2str(kk) '.dat'];
surfinterface=load(load_surfname);
plot(surfinterface(:,5),surfinterface(:,6),'k');

c4=colorbar;

set(q4,'Color','k','ShowArrowHead','off','Visible','on')
set(h4,'HandleVisibility','on','FaceColor',[1 1 1]*0.5,'BaseValue',-1000,'LineStyle','none')
set(c4,'Location','NorthOutside','FontSize',16,'Ticks',[-0.06 0]);
title(c4,'$w$ [m year$^{-1}$]','interpreter','latex')

caxis([-0.06 0])
ylim([-700 400])
set(gca,'box','on','XTick',[-50:10:0]*1000,'XTickLabel',[-50:10:0])
xlabel('$y$ [km]','interpreter','latex')

%%
load('ODE_DATA/permeability_vs_W_v50_B2_flat_bed.mat')
for ii=1:2
subplot(2,2,2+ii)
    plot(-y_New*y_scale/1e3,Gamma_ct_dim+z_b*z_scale,'-.','Color',[0 0.6 0],'LineWidth',4);
end