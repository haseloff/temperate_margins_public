clear all
%close all

set(0,'DefaultFigureWindowStyle','docked')
addpath('/Users/eart0445/Dropbox/iceStreams/BL_NUMERICS/MATLAB_FILES/')
addpath('/home/marianne/Dropbox/iceStreams/BL_NUMERICS/MATLAB_FILES/')

global n gamma accu r W_s_scaled q_0 q_geo_scaled kappa
global z0 z4

% geometric scales
W   = 50.275*1e3; % m, ice stream/ridge half width
W_s = 27*1e3;  % m, ice stream half width
s_0 = 200;    % m, ice stream surface elevation


mu=0.5;
sin_alpha=1e-3;
% other parameters
accu_dim=0.05/(365*24*60*60); % m s^{-1}
A_initial = 2.5*1e-25; % s^{-1} Pa^{-3}
g =9.81; % m/s^2
n=3;
rho  = 910; % kg/m^3
rho_w=1000; % kg/m^3
k   = 2.3; % W m^{-1} K^{-1}
L_h = 334*1e3; % J kg^{-1} = Pa m^3 kg^{-1}

K=1e4;

T_s=-26.5; % Celsius
T_m=0;   % Celsius

q_geo= 70e-3; % W m^{-2}
q_x  = 0; % W m^{-2}

gamma_w=1.8e-3;
k_0=1e-12;

Delta_T=T_m-T_s;

% scales
y_scale = W;

tau_s = rho*g*sin_alpha*y_scale;

z_scale = sqrt(k*Delta_T/( A_initial*tau_s^(n+1) ));

N_scale = rho*g*z_scale;
u_scale = A_initial*tau_s^n*y_scale;
q_scale = A_initial*tau_s^(n+1)*z_scale*y_scale/(rho_w*L_h);

% non-dimensional parameters
accu  = (n+2)/2*accu_dim/(rho*g)^n/A_initial*y_scale^(n+1)/z_scale^(2*n+2);
q_geo_scaled = q_geo/(rho_w*L_h)*y_scale/q_scale;
W_s_scaled   = W_s/y_scale;
gamma = mu/sin_alpha;
r     = rho_w/rho;

melt_scale = (rho_w*L_h)*q_scale/y_scale;

heat_scale = A_initial^(-1/n)*u_scale^(1/n+1)/y_scale^(1/n+1);
viscosity_scale=A_initial^(-1/n)*u_scale^(1/n-1)/y_scale^(1/n-1);

u_scale=u_scale*(365*24*60*60); % m yr^{-1}
y_scale=y_scale*1e-3; % km
N_scale=N_scale*1e-3; % kPa

options = odeset('RelTol',1e-9,'AbsTol',[1 1 1 1 1 1]*1e-10);
warning('off','MATLAB:nearlySingularMatrix');

%% Calculate reference solution
s_c =  s_0/z_scale;
z4 = 200.88/z_scale;
z0 = -727.6/z_scale;

%% Calculate value for kappa=infty

kappa=1e6;

H_C=s_c-z0;
u_C=650/(u_scale);

disp(['%% H_C=' num2str(H_C*z_scale) ', u_C=' num2str(u_C*u_scale)])

diff_u=1;
diff_q=1;

u_bisection=0;
q_bisection=0;

Psi_C_plus  = H_C-r*z0;
Psi_C_minus = 0;

q_0_plus  = .0001;
q_0_minus = -.0001;

q_0=(q_0_plus+q_0_minus)/2;

% First bisection loop
while diff_u>1e-7 && u_bisection<100

    u_bisection=u_bisection+1;
    Psi_C = (Psi_C_plus+Psi_C_minus)/2;

    [y_New,X_New] = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C 0 0],options);

    u_New = X_New(end,2);

    if u_New<=0
        Psi_C_plus  = Psi_C;
    else
        Psi_C_minus = Psi_C;
    end

    diff_u=abs(u_New);

end

disp(['  diff_u=' num2str(diff_u)])

while diff_q>1e-6 && q_bisection<100
    q_bisection=q_bisection+1;

    q_0=(q_0_plus+q_0_minus)/2;

    [y_New,X_New] = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C 0 0],options);

    diff_q=X_New(end,5);

    if diff_q>0
        q_0_plus=q_0;
    else
        q_0_minus=q_0;
    end
    diff_q=abs(diff_q);
end

disp(['  diff_q=' num2str(diff_q)])

H = X_New(:,1);
u = X_New(:,2);
v = X_New(:,3);
Psi_new = X_New(:,4);
q_new = X_New(:,5);
q_int = X_New(end,6);
N_new   = -Psi_new+H+r*z0;

y_inf=y_New;
H_inf = X_New(:,1);
u_inf = X_New(:,2);
v_inf = X_New(:,3);
Psi_inf = X_New(:,4);
q_inf = X_New(:,5);
q_int_inf = X_New(end,6);
N_inf   = -Psi_new+H_inf+r*(z4*y_inf.^4 + z0);
q_0_inf=q_0;

heat_diss_inf=2^(-1/n) * (abs(v_inf./H_inf)).^(n+1);
heat_diss_inf(isnan(heat_diss_inf))=0;

H_ct_inf=H_inf-sqrt(2./heat_diss_inf);
H_ct_inf(heat_diss_inf<=0)=0;
H_ct_inf(H_ct_inf<=0)=0;

deps=1e-7;

Q_ice_inf=1/2*H_inf.*heat_diss_inf-1./H_inf;
Q_ice_inf(H_ct_inf>0)=heat_diss_inf(H_ct_inf>0).*H_ct_inf(H_ct_inf>0);

tau_b_inf = gamma*N_inf;
tau_b_inf(tau_b_inf<0)=0;

melt_rate_inf  = tau_b_inf.*u_inf + Q_ice_inf + q_geo_scaled;


disp(['--> H_C=' num2str(H_C*z_scale) ': u_C=' num2str(u_C*u_scale) ': Psi_C=' num2str(Psi_C) ', q_0=' num2str(q_0)])

%% Reduce kappa
kappa_counter=0;
%
for kappa=10.^[10:-1:0 -0.9:-0.1:-2 -2.01:-0.01:-5]
    kappa_counter=kappa_counter+1;
    dq_0 = sqrt(eps);
    dPsi = sqrt(eps);
    
    % order of variables: [H u v Psi q]
    diff=1;
    Newton_counter=0;
    disp(['%% kappa=' num2str(kappa)])
    disp(['   q_0=' num2str(q_0) ', Psi_C=' num2str(Psi_C) ', u_C=' num2str(u_C*u_scale) ', diff=' num2str(diff)])
        
    while (diff>1e-6 && Newton_counter<20)

        dq_0  = sqrt(eps);
        dPsi_C= sqrt(eps);

        [y_0,X_0]   = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C 0 0],options);
        q_0=q_0+dq_0;
        [y_q,X_q]   = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C 0 0],options);
        q_0=q_0-dq_0;
        [y_Psi,X_Psi]   = ode23s(@hydro_ODE_v50_full_kappa_d,[0 1],[H_C u_C 0 Psi_C+dPsi_C 0 0],options);

        f=[X_0(end,2)
           X_0(end,5)];

        Jaco=[ (X_q(end,2)-X_0(end,2))/dq_0   (X_Psi(end,2)-X_0(end,2))/dPsi_C 
               (X_q(end,5)-X_0(end,5))/dq_0   (X_Psi(end,5)-X_0(end,5))/dPsi_C];

        delta=-Jaco\f;

        q_0   = q_0   + delta(1);
        Psi_C = Psi_C + delta(2);

        diff=norm(f);
        disp(['   q_0=' num2str(q_0) ', q_int=' num2str(X_0(end,6)) ', Psi_C=' num2str(Psi_C) ', diff=' num2str(diff)])
    
        Newton_counter=Newton_counter+1;
        
    end
    if  isnan(diff) || Newton_counter==20
        break
    end
    if diff<=1e-6
        H = X_0(:,1);
        z_b  = z4*y_0.^4 + z0;
        u = X_0(:,2);
        v = X_0(:,3);
        Psi = X_0(:,4);
        q = X_0(:,5);
        q_int = X_0(end,6);
        N   = -Psi+H+r*z_b;
        y=y_0;
    end
    kappa_array(kappa_counter) = kappa;
    Psi_C_array(kappa_counter) = Psi_C;
    q_0_array(kappa_counter)   = q_0;
    q_int_array(kappa_counter) = X_0(end,6);
    N_C_array(kappa_counter)   = -Psi_C+H_C+r*z0;

    figure(1)
    subplot(3,1,1)
    semilogx(kappa,X_0(end,6),'.k','LineWidth',2); hold on
    xlabel('$\kappa$','interpreter','latex')
    ylabel('$\int q_x$ d$y$','interpreter','latex')

    subplot(3,1,2)
    semilogx(kappa,q_0,'.k','LineWidth',2); hold on
    xlabel('$\kappa$','interpreter','latex')
    ylabel('$q_0$','interpreter','latex')

    subplot(3,1,3)
    semilogx(kappa,-Psi_C+H_C+r*z0,'.k','LineWidth',2); hold on
    xlabel('$\kappa$','interpreter','latex')
    ylabel('$N_c$','interpreter','latex')

    if mod(kappa_counter,50)==0
        figure(2)
        subplot(3,2,1)
        plot(-y_0,X_0(:,1))
        xlabel('y'); ylabel('H')

        subplot(3,2,2)
        plot(-y_0,X_0(:,2))
        xlabel('y'); ylabel('u')

        subplot(3,2,3)
        plot(-y_0,-X_0(:,3))
        xlabel('y'); ylabel('v')

        subplot(3,2,4)
        plot(-y_0,X_0(:,4))
        xlabel('y'); ylabel('\Psi')

        subplot(3,2,5)
        plot(-y_0,-X_0(:,5))
        xlabel('y'); ylabel('q_y')

        subplot(3,2,6)
        plot(-y_0,-X_0(:,4)+X_0(:,1)+r*(z4*y_0.^4 + z0))
        xlabel('y'); ylabel('N')

        pause(0.1)
        print2name=['permebility_vs_W_v50_B2_full_kappa_d_' num2str(kappa_counter) '.jpg'];
        print('-djpeg',print2name)
    end
end

figure(1)
subplot(3,1,1)
hold off
semilogx(kappa_array,q_int_array,'-k','LineWidth',2)
xlabel('$\kappa$','interpreter','latex')
ylabel('$\int q_x$ d$y$','interpreter','latex')

subplot(3,1,2)
hold off
semilogx(kappa_array,q_0_array,'-k','LineWidth',2)
xlabel('$\kappa$','interpreter','latex')
ylabel('$q_0$ d$y$','interpreter','latex')

subplot(3,1,3)
hold off
semilogx(kappa_array,N_C_array,'-k','LineWidth',2)
xlabel('$\kappa$','interpreter','latex')
ylabel('$N_c$','interpreter','latex')

save('ODE_DATA/permeability_vs_W_v50_B2_full_kappa_d_deps_1e-4.mat')
%% Assign values

%N(N<=sqrt(eps))=sqrt(eps);
heat_diss=2^(-1/n) * (abs(v./H)).^(n+1);
viscosity=2^(-1/n) .* (abs(v./H)).^(1-n);
heat_diss(isnan(heat_diss))=0;

H_ct=H-sqrt(2./heat_diss);
H_ct(heat_diss<=0)=0;
H_ct(H_ct<=0)=0;

deps=1e-7;

Q_ice=1/2*H.*heat_diss-1./H;
Q_ice(H_ct>0)=heat_diss(H_ct>0).*H_ct(H_ct>0);

Gamma_ct= H_ct;

tau_b = gamma*N;
tau_b(tau_b<0)=0;

melt_rate  = tau_b.*u + Q_ice + q_geo_scaled;

%%
h_w=1; % in m
eta_w=1.8e-3;% in Pa s
N_0=1e6; % in Pa
k_d=kappa_array(end)*eta_w/h_w*(N_0/(rho*g*z_scale))^(-3)*A_initial*tau_s^(n+1)*z_scale^2/rho_w/L_h/rho/g;
disp(['k_d=' num2str(k_d) ' m^2$'])
%% Make final plots
figure(4)
p = panel();
p.pack([1.5 1 1 1 1 1]/6.5,1);
    p(1,1).select();
    hold off
    a1=area(-y*y_scale,z_b*z_scale); hold on
    plot(-y*y_scale,(H+z_b)*z_scale,'-k','LineWidth',2); 
    plot(-y*y_scale,(H_ct+z_b)*z_scale,'-r','LineWidth',2); 

    set(a1,'BaseValue',-2000,'FaceColor',[1 1 1]*0.2)
    ylim([min(z_b) max(H+z_b)]*1.1*z_scale)
    xlim([-1 0]*W/1e3)
    ylabel('$z$ [m]','interpreter','latex')
    t1=title('Effect of finite bed permeability $k_d$');
    set(t1,'interpreter','latex')

    p(2,1).select();
    hold off
    plot(-y*y_scale,u*u_scale,'-k','LineWidth',2); hold on
    plot(-y_inf*y_scale,u_inf*u_scale,'--b','LineWidth',2); hold off
    
    ylim([0 1]*max(u*u_scale)*1.1)
    xlim([-1 0]*W/1e3)
    ylabel({'Velocity' '$u$ [m yr$^{-1}$]'},'interpreter','latex')

    p(3,1).select();
    plot(-y*y_scale,N*N_scale,'-k','LineWidth',2); hold on
    plot(-y_inf*y_scale,N_inf*N_scale,'--b','LineWidth',2); hold off
    
    ylim([0 1]*max(N_inf*N_scale)*1.1)
    xlim([-1 0]*W/1e3)
    ylabel({'Effective' 'pressure' '$N$ [kPa]'},'interpreter','latex')


    p(4,1).select();
    hold off
    plot(-y*y_scale,melt_rate*melt_scale*1000,'-k','LineWidth',2); hold on
    plot(-y_inf*y_scale,melt_rate_inf*melt_scale*1000,'--b','LineWidth',2); hold off
  %  plot(-y_0*y_scale,tau_b.*u*melt_scale*1000,'-.b','LineWidth',1)
  %  plot(-y_0*y_scale,Q_ice*melt_scale*1000,'-.r','LineWidth',1); hold off
    ylabel({'Melt rate' '[$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
    xlim([-1 0]*W/1e3)
  %  l2=legend('$\rho_w L_h (\dot m + j_b)$','$\rho_w L_h \dot m$','$\rho_w L_h j_b$');
  %  set(l2,'Location','NorthWest','interpreter','latex','box','off')

    p(5,1).select();
    plot(-y*y_scale,-q*melt_scale*1000,'-k','LineWidth',2); hold on
    plot(-y_inf*y_scale,-q_inf*melt_scale*1000,'--b','LineWidth',2); hold off
    ylabel({'Downstream' 'water flux' '$$\frac{\partial q_x}{\partial x}$$ [$10^{-3}$ m yr$^{-1}$]'},'interpreter','latex')
    xlim([-1 0]*W/1e3)

    p(6,1).select();
    plot(-y*y_scale,-q_0./(sqrt(eps)+N).^3*melt_scale*1000,'-k','LineWidth',2); hold on
    plot(-y_inf*y_scale,-q_0_inf./(sqrt(eps)+N_inf).^3*melt_scale*1000,'--b','LineWidth',2); hold off
    xlim([-1 0]*W/1e3)
    xlabel('$y$ [km]','interpreter','latex')
    ylabel({'Lateral' 'water flux' '$q_y$ [m$^2$ yr$^{-1}$]'},'interpreter','latex')
    
% Plot temperature field

    H_dim = H*z_scale;
    s_dim = H_dim;
    H_ct_dim     = H_ct*z_scale;
    Gamma_ct_dim = H_ct_dim;
    heat_diss_dim=heat_diss*heat_scale;
    viscosity_dim=viscosity*viscosity_scale;
    N_dim = N*N_scale;

    deps=1e-7;


    deltaZ=0.0001;
    z_temp=[0:deltaZ:1];
    [Y,Z]=meshgrid(y_0,z_temp);
    Y=Y*y_scale;

    THICKNESS_dim = NaN*Z;
    GAMMA_dim = NaN*Z;
    HEAT_dim = NaN*Z;
    TEMP_dim = NaN*Z;
    PHI_dim = NaN*Z;
    PE_dim = NaN*Z;


    for i=1:length(y_0)
        Z(:,i)=H_dim(i)*Z(:,i);
        S(:,i)=H_dim(i) + 0*Z(:,i);close all
        
        GAMMA_dim(:,i)=Gamma_ct_dim(i) + 0*Z(:,i);
        THICKNESS_dim(:,i)=H_dim(i) + 0*Z(:,i);
        HEAT_dim(:,i)=heat_diss_dim(i) + 0*Z(:,i);
        if Gamma_ct_dim(i)>0
            for j=1:length(z_temp)
                if z_temp(j)*H_dim(i)>=Gamma_ct_dim(i)
                    TEMP_dim(j,i)=T_s + HEAT_dim(j,i)/k.*((S(j,i).^2-Z(j,i).^2)/2-GAMMA_dim(j,i).*(S(j,i)-Z(j,i))  );
                    %PHI_dim(:,i)  = 0;
                else
                    TEMP_dim(j,i)=T_m;
                end
            end   
        else
            TEMP_dim(:,i) = T_s + (T_m-T_s)*(S(:,i)-Z(:,i))./THICKNESS_dim(:,i)+ HEAT_dim(:,i)/(2*k).*(  (S(:,i)-Z(:,i)).*Z(:,i)  );
            %PHI_dim(:,i)  = 0;
        end
    end
    
    for i=1:length(y_0)
        Z(:,i)=Z(:,i)+z_b(i)*z_scale;
    end
    p(1,1).select();
    contourf(-Y,Z,TEMP_dim,'LineStyle','none'); hold on
    plot(-y_0*y_scale,Gamma_ct_dim+z_b*z_scale,'r','LineWidth',2); hold off
    c1=colorbar;
    set(c1,'Location','eastoutside','FontSize',14,'FontName','Times')
    title(c1,'$T$ [$^o$C]','FontSize',14,'interpreter','latex')
    load('temp_colormap.mat');
    set(gcf,'Colormap',cmap); 
    caxis([-Delta_T 0])
    ylim([-750 450])

p(1,1).select();
text(-7,350,['(a)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','layer','top','XTickLabel',[])
l1=legend('$k_d= ???$','k_d=\infty');

p(2,1).select();
text(-7,750,['(b)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','XTickLabel',[])

p(3,1).select();
text(-7,1400,['(c)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','XTickLabel',[])

p(4,1).select();
text(-7,47,['(d)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','XTickLabel',[])

p(5,1).select();
text(-7,200,['(f)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','XTickLabel',[])

p(6,1).select();
text(-7,27,['(e)'],'FontSize',16,'FontName','Times','interpreter','latex')
set(gca,'box','on','XTickLabel',[])
%% Set margins
p.de.margin = 2;
p.de.marginbottom = 5;
p.de.marginright  = 5;

% left bottom right top
p.margin = [35 20 33 15];
%p.select('all');

p.fontsize = 16;
p.fontname = 'Times';

set(gcf,'PaperPositionMode','auto')
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
fig1=gcf;
print -dpdf permeability_vs_W_v50_B2_full_kappa_d_deps_1e-4.pdf