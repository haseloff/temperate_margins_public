%1:3 Velocity
%4:  Pressure scalar: 
%5:  W scalar
%6:  Temp scalar
%7:  Temp.homologous scalar
%8:  Temp.residual scalar
%9:  Temp.loads scalar
%10: Dtdz
u_scale=365*24*60*60;

    if kk<10
        dataID=['00' num2str(kk)];
    else
        dataID=['0' num2str(kk)];
    end
    nameImport=['ELMER_DATA/mesh3d_za/stream_Pe_'  dataID '.ep'];
    [a b]=grep('-n -e', '#time',nameImport);
    
    A = importdata(nameImport,' ',2);
    coordinates=A.data;

    B = importdata(nameImport,' ',b.line(end));
    variables=B.data;

    Y = (-50:0.1:0)*1000;
    Z=  [-750.2:1:364.1];
    [yq,zq] = meshgrid(Y,Z);
        
    coordinates_x(:,1)=coordinates(~isnan(coordinates(:,1)),1 );
    coordinates_y(:,1)=coordinates(~isnan(coordinates(:,2)),2 );
    coordinates_z(:,1)=coordinates(~isnan(coordinates(:,3)),3 );
    variables_u_x(:,1)=variables(~isnan(variables(:,1)),4 );
    
    u_velo = griddata(coordinates_y,coordinates_z,variables_u_x,yq,zq,'linear');
    
    hold off    
    b1=area([min(Y) max(Y)],[min(Z) min(Z)],'LineWidth',2,'HandleVisibility','off'); hold on
    set(b1,'HandleVisibility','off','FaceColor',[1 1 1]*0.5)
    contourf(yq,zq,u_velo*u_scale,[0:0.1:1 2:1:10 20:10:700],'LineStyle','none'); hold on
    con1=contour(yq,zq,u_velo*u_scale,[0.01 100 500],'k','LineWidth',1); hold on
    con2=contour(yq,zq,u_velo*u_scale,[1 1],'k','LineWidth',2); hold on
 %plot([y 5],[z 1],'w','LineWidth',2)
    
    load_surfname=['ELMER_DATA/surface_00' num2str(kk) '.dat'];
    surfinterface=load(load_surfname);

    h1=area(surfinterface(:,5),surfinterface(:,6));
    set(h1,'BaseValue',400,'FaceColor','w','HandleVisibility','off')
    